find_package(PNG REQUIRED)
include_directories(${PNG_INCLUDE_DIRS})

add_catch(test_fast_dither test.cpp)
target_link_libraries(test_fast_dither ${PNG_LIBRARY})
