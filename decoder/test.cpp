#include <catch.hpp>
#include <decoder.h>

#include <string>
#include <iostream>

std::string ReadText(const std::string& filename) {
    char current;
    std::string text;
    std::ifstream fin(filename);
    while (fin.get(current)) {
        text += current;
    }
    return text;
}

static auto dictionary = ReadNgrams("../decoder/english_quadgrams.txt");

TEST_CASE("Your test") {
}
