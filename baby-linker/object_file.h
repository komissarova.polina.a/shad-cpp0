#pragma once

#include <vector>
#include <string>

enum class ObjectType {
    RODATA, // RO stands for read-only
    CODE
};

struct LinkerArgument {
    virtual ~LinkerArgument() {}
};

struct Object {
    ObjectType type;
    std::string symbol_name;

    size_t offset;
    size_t size;
};

struct ObjectFile : public LinkerArgument {
    std::vector<Object> objects;
    std::vector<std::string> references;

    std::string data_segment;
    std::string code_segment;
};

struct StaticArchive : public LinkerArgument {
    std::vector<ObjectFile> object_files;
};

struct SharedLibrary : public LinkerArgument {
    std::string name;

    std::vector<Object> objects;
    std::vector<std::string> references;
    std::vector<std::string> libraries;

    std::string data_segment;
    std::string code_segment;
};

struct Executable {
    std::vector<Object> objects;
    std::vector<std::string> libraries;

    std::string data_segment;
    std::string code_segment;
};