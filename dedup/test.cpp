#include <dedup.h>

#include <catch.hpp>

TEST_CASE("Duplicating simple ints") {
    auto i0 = std::make_shared<int>(0);
    auto i1 = std::make_shared<int>(1);
    auto i2 = std::make_shared<int>(2);

    std::vector<std::shared_ptr<int>> shared = {i0, i1, i2, i2, i1, i0};

    auto unshared = Duplicate(shared);

    REQUIRE(*(unshared[0]) == 0);
    REQUIRE(*(unshared[1]) == 1);
    REQUIRE(*(unshared[2]) == 2);
    REQUIRE(*(unshared[3]) == 2);
    REQUIRE(*(unshared[4]) == 1);
    REQUIRE(*(unshared[5]) == 0);

    REQUIRE(unshared[0] != unshared[5]);
    REQUIRE(unshared[1] != unshared[4]);
    REQUIRE(unshared[2] != unshared[3]);

    REQUIRE(unshared[0].get() != i0.get());
    REQUIRE(unshared[1].get() != i1.get());
    REQUIRE(unshared[2].get() != i2.get());
}

TEST_CASE("Deduping simple ints") {
    std::vector<std::unique_ptr<int>> unshared;
    unshared.emplace_back(std::make_unique<int>(0));
    unshared.emplace_back(std::make_unique<int>(1));
    unshared.emplace_back(std::make_unique<int>(2));
    unshared.emplace_back(std::make_unique<int>(2));
    unshared.emplace_back(std::make_unique<int>(1));
    unshared.emplace_back(std::make_unique<int>(0));

    auto shared = DeDuplicate(unshared);
    REQUIRE(*(shared[0]) == 0);
    REQUIRE(*(shared[1]) == 1);
    REQUIRE(*(shared[2]) == 2);
    REQUIRE(*(shared[3]) == 2);
    REQUIRE(*(shared[4]) == 1);
    REQUIRE(*(shared[5]) == 0);

    REQUIRE(shared[0] == shared[5]);
    REQUIRE(shared[1] == shared[4]);
    REQUIRE(shared[2] == shared[3]);
}

class NonComparable {};

TEST_CASE("Duplication is not using value comparison") {
    auto item = std::make_shared<NonComparable>();
    std::vector<std::shared_ptr<NonComparable>> shared = {item, item, item};

    auto unshared = Duplicate(shared);
    REQUIRE(unshared[0] != unshared[1]);
    REQUIRE(unshared[1] != unshared[2]);
}
