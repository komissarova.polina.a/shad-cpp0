#include <catch.hpp>

#include <expr_parser.h>

#include <sstream>

TEST_CASE("Tokenizer works on simple case") {
    std::stringstream ss{"4+)"};
    Tokenizer tokenizer{&ss};

    REQUIRE(!tokenizer.IsEnd());
    REQUIRE(tokenizer.GetToken() == Token{Constant{4}}); // Confused by compilation error? Think harder!

    tokenizer.Next();
    REQUIRE(!tokenizer.IsEnd());
    REQUIRE(tokenizer.GetToken() == Token{Operator::PLUS});

    tokenizer.Next();
    REQUIRE(!tokenizer.IsEnd());
    REQUIRE(tokenizer.GetToken() == Token{Bracket::CLOSE});

    tokenizer.Next();
    REQUIRE(tokenizer.IsEnd());
}

TEST_CASE("GetToken is not moving") {
    std::stringstream ss{"4+4"};
    Tokenizer tokenizer{&ss};

    REQUIRE(tokenizer.GetToken() == Token{Constant{4}});
    REQUIRE(tokenizer.GetToken() == Token{Constant{4}});
}

TEST_CASE("Spaces are handled correctly") {
    SECTION("Just spaces") {
        std::stringstream ss{"      "};
        Tokenizer tokenizer{&ss};

        REQUIRE(tokenizer.IsEnd());
    }

    SECTION("Spaces between tokens") {
        std::stringstream ss{"  4 +  "};
        Tokenizer tokenizer{&ss};

        REQUIRE(!tokenizer.IsEnd());
        REQUIRE(tokenizer.GetToken() == Token{Constant{4}});

        tokenizer.Next();
        REQUIRE(!tokenizer.IsEnd());
        REQUIRE(tokenizer.GetToken() == Token{Operator::PLUS});

        tokenizer.Next();
        REQUIRE(tokenizer.IsEnd());
    }
}

TEST_CASE("Empty string handled correctly") {
    std::stringstream ss;
    Tokenizer tokenizer{&ss};

    REQUIRE(tokenizer.IsEnd());
}