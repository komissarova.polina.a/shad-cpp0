find_package(Boost 1.54 REQUIRED)
include_directories(${Boost_INCLUDE_DIRS})

add_catch(test_news_aggregator test.cpp PRIVATE_TESTS test.cpp)
