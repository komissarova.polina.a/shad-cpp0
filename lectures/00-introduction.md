# Лекция 1. Введение в C++

Фёдор Короткий

---

# Вы пришли на курс С++

 - Занятия по понедельникам
 - Лекции и семинары

---

# Задания в курсе

 - 12+ семинаров, дедлай 10 дней
 - 4 домашних задания, дедлайн 2-3 недели

---

# Критерии оценки

 - **Зачёт:** 2 ДЗ + 60% за семинары или 3 + 50%
 - **Хорошо:** 2 + 80% или 3 + 70% или 4 + 60%
 - **Отлично:** 3 + 90% или 4 + 80%

---

# Телеграм чат

 - Есть у вас есть вопрос или возникла какая-то проблема - Задавайте вопрос в чате.
 - Флуд не приветствуется
 - Логи, вывод компилятора и большие куски кода заливайте на gist.github.com

---

# Бъерн Страуструп - Создатель С++

![](00-introduction/straustrup.jpg)

---

# Стандарт С++

 * Описание языка и стандартной библиотеки
 * Создан комитетом по стандартизации

---

# История стандартов C++

## с++98
Вам не встретится.

## с++11
Добавил очень много вещей. Язык сильно поменялся и улучшился.

## с++14
Небольшие правки. Можно назвать `c++11.1`.

## с++17
Последний принятый стандарт. Косметика.

---

# Компиляторы

 * **gcc** - основной компилятор на linux, самый старый, лицензия GPL.
 * **clang** - основной конкурент gcc, лицензия MIT привлекает компании. Основной компилятор на OSX и iOS.
 * **mvcc** - основной компилятор на Windows.
 * **icc** - компилятор от Intel, закрытый.

Компиляторы одновременно поддерживают несколько версий стандарта. `-std=c++14`

---

# Выучить С++?

 - Язык очень массивный (1448 страниц в стандарте)
 - Многие части языка реализованы в библиотеках
 - Каждый крупный проект использует подмножество языка
 - Вам не нужно знать весь С++ чтобы успешно его применять

---

# Hello World

```c++
#include <iostream> // ввод-вывод

// Функция, с которой начинается исполнение программы.
int main() { 
    std::cout << "Hello World" << std::endl;
    return 0; // код возврата всей программы
}
```

---

# Переменные

```с++
int my_variable = 0;
```

---

# Статическая типизация

 - С++ - язык со статической типизацией
 - Типы защищают вас от ошибок

---

# Примитивные типы

```
bool the_cake_is_a_lie = true;
int answer_to_the_question = 42;
size_t unsigned_number = 1U;
uint64_t quite_big_number = -1ULL;

float floating_point_number = 0.5;
double double_precision_floating_point_number = 1e9;
```

---

# Ввод/вывод `<iostream>`

```c++
int a, b;
std::cin >> a >> b;
std::cout << (a + b) << std::endl;
```

```
1 2
3
```

---

# Логические операции

```c++
bool v = (!true || false) && (0 != 2);
bool g = 1 > 15;
```

---

# Short-circuit evaluation

 - `&&` и `||` вычисляются по особым правилам

```c++
// оптимизация логических выражений
bool b = president_is_alive || launch_nukes();
```

---

# Функции

```c++
int sum(int a, int b) {
    int c = a + b;
    return c;
}
```

---

# Функции

```
// Определение (definition)
int sum(int a, int b) {
    int c = a + b;
    return c;
}

// Объявление (declaration)
int square(int b);

void f() {
    std::cout << sum(1, 2) << std::endl;    
    int a = square(-1);
    return;
}
```

---

# Оператор if

```c++
void PrintSign(int i) {
    if (i > 0) {
        std::cout << "positive" << std::endl;
    } else if (i < 0) {
        std::cout << "negative" << std::endl;
    } else {
        std::cout << "zero" << std::endl;
    }
}
```

---

# Пользовательские типы

 * В язык "встроены" только примитивные типы
 * Все остальные созданы пользователями языка

---

# `std::vector<T>`

 - Массив элементов типа `T`
 - Обращение к элементу по индексу за O(1)
 - Добавление элемента в конец за O(1)

---

# `std::vector`

```c++
std::vector<int> years;
years.push_back(2018);
years.push_back(2019);

years[0] = 1812;

std::cout << years.size() << std::endl;

years.pop_back();

std::cout << years.size() << std::endl;
```

---

# Циклы

```c++
void PrintVector(std::vector<int> some_numbers) {
    for (size_t i = 0; i < some_numbers.size(); ++i) {
        std::cout << some_numbers[i] << std::endl;
    }
}

std::vector<int> Remove42Suffix(std::vector<int> v) {
    while (!v.empty() && v.back() == 42) {
        v.pop_back();
    }
    return v;
}
```

---

# Range-based for

```c++
void PrintVector(std::vector<int> some_numbers) {
    for (int number : some_numbers) {
        std::cout << number << std::endl;
    }
}
```

---

# auto и вывод типов

Ключевое слово `auto` позволяет не писать тип

```с++
std::vector<int> x;

auto y = x;
// y имеет тип std::vector<int>
```

---

# `std::map`

```c++
std::map<std::string, int> word_counts;
word_counts["The"] = 10000;
word_counts["a"] = 50;

word_counts["a"] == 50;
word_counts["b"] == 0; // ???

// std::map<std::string, int>::iterator
auto it = word_counts.find("c");
if (it != word_counts.end()) {
    std::cout << it->first << " "
              << it->second << std::endl;
}

word_counts.erase("The");
word_counts.size() == 2;
```

---

# `std::string`

```c++
std::string name = "Fedor";
std::string nick = "prime";
name.size() == 5;
name[1] == 'e';

std::string display_name = name + " aka " + nick;
```

---

# Что нужно запомнить

 - `std::vector`, `std::map`, `std::string`
 - `auto` - ваш друг
 - https://cppreference.com
