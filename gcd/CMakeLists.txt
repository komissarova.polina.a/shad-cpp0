cmake_minimum_required(VERSION 2.8)
project(gcd)

include(../common.cmake)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O0")

add_executable(gcd gcd.cpp)
