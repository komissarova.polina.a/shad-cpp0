cmake_minimum_required(VERSION 2.8)
project(fraction)

include(../common.cmake)

add_executable(test_fraction test.cpp)
