#!/bin/bash

# Run from build directory

if [ "$#" -ne 1 ]; then
    echo "Usage: $0 task-name"
    exit 1
fi

TASK=$1

../run-clang-format.py -r ../$TASK && clang-tidy ../$TASK/*.cpp
