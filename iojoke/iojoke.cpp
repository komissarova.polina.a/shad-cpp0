#include <cstdlib>
#include <iostream>
#include <clocale>

const int MAX_STRING_NUMBER = 100000;
const int MAX_LENGTH = 10;

bool GetLine(int max_length) {
    while (std::wcin.good() && max_length--) {
        auto symbol = std::wcin.get();
        if (symbol == L'\n')
            return true;
        if (symbol < L'а' || symbol > L'я')
            return false;
    }
    return false;
}

// The BUG is not here. You have to search elsewhere.
int main(void) {
    std::locale ru("ru_RU.UTF-8");
    std::locale::global(ru);

    for (int i = 0; i < MAX_STRING_NUMBER; ++i) {
        bool isValid = GetLine(MAX_LENGTH);
        if (std::wcin.eof())
            break;
        if (!isValid) {
            std::wcout << L"Дай недлинные строки из русских букв" << std::endl;
            while (!std::wcin.eof())
                std::wcin.get();
            exit(0);
        }
    }
    return 0;
}
